//
//  FeedCollectionViewCell.swift
//  Test app
//
//  Created by Ahmad Alnashukati on 2/28/21.
//

import UIKit

class FeedCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var feedDecription: UILabel!
    @IBOutlet weak var feedTitile: UILabel!
    @IBOutlet weak var feedImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    
    func config( feed : FeedModel){
        
        self.feedImage.image = feed.image
        self.feedTitile.text = feed.name
        self.feedDecription.text = feed.description
    }
}
