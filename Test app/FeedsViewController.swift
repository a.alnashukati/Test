//
//  ViewController.swift
//  Test app
//
//  Created by Ahmad Alnashukati on 2/27/21.
//

import UIKit

class FeedsViewController: UIViewController {
    
    
    @IBOutlet weak var searchView: UIView!
    
    @IBOutlet weak var feedsCollectionView: UICollectionView!
    
    @IBOutlet weak var layoutBtn: UIButton!
  
    
    
    private var cellID = "feedCell"
    var feedViewModle = FeedsViewModle()
    var currentLayoutType = LayoutTypes.FixedSize
 
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        feedsCollectionView.register(UINib(nibName: "FeedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: cellID)
        
        feedsCollectionView.delegate = self
        feedsCollectionView.dataSource = self
        feedsCollectionView.dragDelegate = self

        feedsCollectionView.collectionViewLayout = AutomaticSizeLayout()
        if let layout = feedsCollectionView.collectionViewLayout as? AutomaticSizeLayout {
          layout.delegate = self
        }
        //feedsCollectionView.collectionViewLayout = UICollectionViewFlowLayout()

     //   currentLayoutType = .FixedSize
    
        layoutBtn.addTarget(self, action: #selector(layoutBtnAction), for: .touchUpInside)
        
        feedViewModle.getData {
            self.feedsCollectionView.reloadData()
        }
        
    }
    
    
    
    
    @objc func layoutBtnAction(){
        switch currentLayoutType {
      
        case .AutomaticSize:
            feedsCollectionView.collectionViewLayout = UICollectionViewFlowLayout()
            currentLayoutType = .FixedSize
             
            
        case .FixedSize:
            feedsCollectionView.collectionViewLayout = AutomaticSizeLayout()
            if let layout = feedsCollectionView.collectionViewLayout as? AutomaticSizeLayout {
              layout.delegate = self
            }
            currentLayoutType = .AutomaticSize
        }
       
        feedsCollectionView.reloadData()

        
    }


}




extension FeedsViewController : UICollectionViewDragDelegate {
  
    
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        
        let item = feedViewModle.list[indexPath.row]
        let provider = NSItemProvider(object: item.name! as NSString)
        let dragItem = UIDragItem(itemProvider: provider)
        dragItem.localObject = item
        return [dragItem]
    }
    
    

    
   

    
 
    
}

extension FeedsViewController : UICollectionViewDelegate , UICollectionViewDataSource , UICollectionViewDelegateFlowLayout {
  
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return   feedViewModle.list.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell  = collectionView.dequeueReusableCell(withReuseIdentifier: cellID, for: indexPath) as! FeedCollectionViewCell
        
        cell.config(feed:feedViewModle.list[indexPath.item] )
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: collectionView.frame.width * 0.5 - 20, height: 150)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    }
    
}




extension FeedsViewController: AutomaticSizeLayoutDelegate {
  
    
    func collectionView(_ collectionView: UICollectionView, heightForPhotoAtIndexPath indexPath: IndexPath) -> CGFloat {
        
        print( feedViewModle.list[indexPath.item].image?.size.height)
        return  (feedViewModle.list[indexPath.item].image?.size.height) ?? 200
    }
    
    
    
    
}





enum  LayoutTypes:String {
    case FixedSize , AutomaticSize
}
