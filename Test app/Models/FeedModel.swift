//
//  FeedModel.swift
//  Test app
//
//  Created by Ahmad Alnashukati on 2/27/21.
//

import UIKit
import  SwiftyJSON
import Kingfisher
class FeedModel {
    
    var name : String?
    var description:String?
    var title:String?
    var image :UIImage?
    
    init( data : JSON ) {
    
        let urlToImage = data["urlToImage"].string ?? ""
        if let imageUrl = URL(string: urlToImage ){
            KingfisherManager.shared.retrieveImage(with: imageUrl, options: nil, progressBlock: nil) { result in
                       switch result {
                       case .success(let value):
                        self.image = value.image
                       case .failure:
                        self.image  = nil
                       }
                   }

        }
        
        self.title = data["title"].string
        self.description = data["description"].string
        let source = data["source"]
        self.name  = source["name"].string
        
        
        
        
    }
    
}
