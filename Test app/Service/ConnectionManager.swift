//
//  ConnectionManager.swift
//  Test app
//
//  Created by Ahmad Alnashukati on 2/27/21.
//

import Foundation
import SwiftyJSON
class ConnectionManager {
    
    private init (){
        
        
    }
    
    private let url = "http://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=a523d10d6bdd4d36a98936d9259dd3ad"
    
    static let shared = ConnectionManager()
    
    func getData(completion: @escaping ([FeedModel]?)->()){
        
        guard  let apiUrl = URL(string: url) else {
            return
        }
      
        
        URLSession.shared.dataTask(with: apiUrl) { (data, response, erroe) in
            if let data = data {
                
                let json  = JSON(data)
                var feedsList = [FeedModel]()
                if    let articles = json["articles"].array {
                    
                    for item in articles {
                        let feed = FeedModel(data: item)
                        feedsList.append(feed)
                    }
                    completion(feedsList)
                }
                
            }else {
                
                completion(nil)

            }
            
        }.resume()
        
        
        
    }
}
