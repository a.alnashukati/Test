//
//  FeedsViewModle.swift
//  Test app
//
//  Created by Ahmad Alnashukati on 2/27/21.
//

import Foundation
class FeedsViewModle {
    
    var list = [FeedModel]()
    
   
    
    
    init() {
    }
    
    
    func getData(completion: @escaping ()->()){
        
        ConnectionManager.shared.getData { (list) in
            
            if let list = list {
                self.list = list
                
                DispatchQueue.main.async {
                    completion()

                }
            }
        
        }
    }
    
    
}
